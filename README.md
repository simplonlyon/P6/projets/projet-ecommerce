# projet-ecommerce

Par groupe de 3, créer un projet Symfony 4 de vente en ligne pour le type de produit que vous souhaiter.
Les fonctionnalités minimales attendues sont : la gestion des utilisateur·ice·s, la gestion du panier, l'affichage des produits (et leur mise au panier),
éventuellement un backoffice pour permettre à un·e administrateur·ice d'ajouter/supprimer/gérer les produits.

* Faire les diagrammes et maquetter l'application
    * Faire un diagramme de Use case
    * Faire un diagramme de classe représentant les entités
    * Faire une maquette des différents écrans de l'application en wireframe
    * Faire un diagramme d'activité pour lié les écrans
    * Optionnel : faire un diagramme de classe pour l'appli complète, contrôleurs/repository/etc. et faire un diagramme de séquence pour un scénario
* Faire un projet Symfony 4 avec ORM
    * Créer les entités et repository Doctrine (via le cli)
    * Modifier les repository pour mettre toutes les requêtes prévues
    * Créer les classes formulaires pour les entités
    * Gérer l'authentification symfony (user qui implémente la bonne interface, yaml, etc.)
    * Créer les templates et contrôleurs pour les différentes pages

## Instructions pour la mise en ligne du projet blog sur simplonlyon.fr :
Les parties avec un ~ devant sont pour les personnes qui n'ont pas de fixtures
I. Côté local (sur votre machine à vous)
1. S'assurer que ça fonctionne bien comme il faut, sur toutes les pages, en local
2. ~ Faire un export de toute votre base de données avec MySQL Workbench en cochant la case  self-contained file (et décocher "include create schema"). Mettre le .sql obtenu à la racine du dossier de votre projet.
3. Faire un commit et un push de tous vos trucs, de préférence sur le gitlab commun
4. Se connecter en ssh à simplonlyon.fr (`ssh username@simplonlyon.fr`)

II.Côté Serveur (sur le terminal connecté en SSH)
1. Aller dans votre dossier www
2. Faire un gitclone de votre projet
3. Renommer le dossier obtenu par le clone en "ecommerce" (important, sinon ça ne marchera pas)
4. Aller dans le dossier ecommerce
5. Faire un composer install
6. Se connecter au serveur SQL de simplonlyon avec la commande : `mysql -u username -p`               Votre username est votre nom d'utilisateur.ice simplonlyon et votre mot de passe également
7. Créer la base de donnée avec la commande : `CREATE DATABASE username_ecommerce;`                Remplacez username par votre nom d'utilisateur.ice simplonlyon
8. Quitter mysql avec la commande : `exit`
9. ~ Importer la base de donnée avec la commande :  `mysql -u username -p username_ecommerce < nom_fichier.sql`              Remplacez username par votre nom d'utilisateur.ice simplonlyon et nom_fichier par le nom de votre fichier SQL généré précédemment
10. Créer/modifier un fichier .env avec nano et dedans mettre (au dessus de MYSQL_URL si il y a) :
`MYSQL_HOST=localhost
MYSQL_USER=username
MYSQL_PASSWORD=username
MYSQL_DATABASE=username_ecommerce`  
En remplaçant bien à chaque fois username par votre nom d'utilisateur.ice simplonlyon
11. Si vous utiliser webpack Encore etc. il faut faire également un `npm install` et un `npm run build`
12. Essayez d'accéder à votre projet voir si ça marche bien pour toutes les pages/formulaires
13. Si oui, rajouter dans le .env la ligne : `APP_ENV=prod` (mais ça c'est vraiment quand tout tout tout est ok)
